﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    public AudioSource soundMusic;
    public AudioSource soundFX;
    public AudioClip jumpClip, gameOverClip, superClip;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        soundMusic.volume = DataManager.Instance.LoadGame().SoundMusic;
        soundFX.volume = DataManager.Instance.LoadGame().SoundFX;
    }

    public void JumpSoundFX()
    {
        soundFX.clip = jumpClip;
        soundFX.Play();
    }

    public void GameOverSoundFX()
    {
        soundFX.clip = gameOverClip;
        soundFX.Play();
    }

    public void SuperSoundFX()
    {
        soundFX.clip = superClip;
        soundFX.Play();
    }
}

