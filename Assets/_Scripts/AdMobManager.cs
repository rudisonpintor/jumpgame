﻿using GoogleMobileAds.Api;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdMobManager : MonoBehaviour
{
    public static AdMobManager Instance;
    private BannerView bannerView;

    [SerializeField] private string appID = "ca-app-pub-7673492683137581~5782632617";
    [SerializeField] private string bannerID = "ca-app-pub-7673492683137581/1774448301";
    [SerializeField] private string regularAD = "ca-app-pub-7673492683137581/6904142590";
    [SerializeField] private string regularADPremiado = "ca-app-pub-7673492683137581/7042963141";
    private RewardBasedVideoAd advideo;
    private InterstitialAd insterstialAd;

    //ca-app-pub-7673492683137581/1774448301 // BannerBottom, nome = BannerBottomGame
    //ca-app-pub-7673492683137581/6904142590 // Video Normal, VideoGameOver
    //ca-app-pub-7673492683137581/7042963141 // video premiado, nome = GameOverPremiado
    //ca-app-pub-7673492683137581~5782632617// APP ID

    private void Awake()
    {
        //Singleton();

        MobileAds.Initialize(appID);

        //RegularVideoAd();
        BannerRequest();
    }

    //void Singleton()
    //{
    //    if (Instance != null)
    //    {
    //        Destroy(gameObject);
    //    }
    //    else if (Instance == null)
    //    {
    //        Instance = this;
    //        DontDestroyOnLoad(gameObject);
    //    }
    //}

    public void OnClickShowBanner()
    {
        bannerView.Show();
    }

    public void OnClickShowBannerClose()
    {
        bannerView.Hide();
    }


    public void OnClickShowAd()
    {
        RegularVideoAd();
    }

    public void OnClickShowVideoPremiado()
    {
        if (advideo.IsLoaded())
            advideo.Show();
    }

    private void RequestRegular()
    {
        insterstialAd = new InterstitialAd(regularAD);

        AdRequest adRequest = new AdRequest.Builder().Build();

        insterstialAd.LoadAd(adRequest);

        insterstialAd.Show();
    }

    /// <summary>
    /// Video
    /// </summary>
    private void RegularVideoAd()
    {
        advideo = RewardBasedVideoAd.Instance;

        AdRequest ad = new AdRequest.Builder().Build();

        advideo.LoadAd(ad, regularADPremiado);
    }

    private void BannerRequest()
    {
        bannerView = new BannerView(bannerID, AdSize.Banner, AdPosition.Bottom);

        //Cliente
        AdRequest adRequest = new AdRequest.Builder().Build();

        bannerView.LoadAd(adRequest);
    }

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        OnClickShowBanner();
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        BannerRequest();
        //OnClickShowBannerClose();
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {

    }

    void HandleBannerADEvents(bool subscribe)
    {
        if (subscribe)
        {
            // Called when an ad request has successfully loaded.
            bannerView.OnAdLoaded += HandleOnAdLoaded;
            // Called when an ad request failed to load.
            bannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;
            // Called when an ad is shown.
            bannerView.OnAdOpening += HandleOnAdOpened;
            // Called when the ad is closed.
            bannerView.OnAdClosed += HandleOnAdClosed;
            // Called when the ad click caused the user to leave the application.
            bannerView.OnAdLeavingApplication += HandleOnAdLeavingApplication;
        }
        else
        {
            // Called when an ad request has successfully loaded.
            bannerView.OnAdLoaded -= HandleOnAdLoaded;
            // Called when an ad request failed to load.
            bannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;
            // Called when an ad is shown.
            bannerView.OnAdOpening -= HandleOnAdOpened;
            // Called when the ad is closed.
            bannerView.OnAdClosed -= HandleOnAdClosed;
            // Called when the ad click caused the user to leave the application.
            bannerView.OnAdLeavingApplication -= HandleOnAdLeavingApplication;
        }
    }

    private void OnEnable()
    {
        HandleBannerADEvents(true);
    }

    private void OnDisable()
    {
        HandleBannerADEvents(false);
    }

}
