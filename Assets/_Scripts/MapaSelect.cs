﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapaSelect : MonoBehaviour
{
    public static MapaSelect Instance;

    public GameObject[] mapasPrefab;
    private int currentIndex;
    public GameObject btnComprar;
    public Image iconPreco;
    public Text txtPreco;
    public Text txtComprado;
    public Sprite spriteComprado, spriteSelecionar;
    private bool[] _mapas;
    private Animator btnComprarAnimator;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        InitializeComponentes();
        btnComprarAnimator = btnComprar.GetComponent<Animator>();
    }

    /// <summary>
    /// Metodo para carregar os mapas liberados
    /// </summary>
    void InitializeComponentes()
    {
        currentIndex = Home.Instance.selectedIndexMapa;

        for (int i = 0; i < mapasPrefab.Length; i++)
        {
            mapasPrefab[i].SetActive(false);
        }

        mapasPrefab[currentIndex].SetActive(true);
        _mapas = Home.Instance.mapas;
    }

    /// <summary>
    /// Metodo para selecionar o mapa posterior
    /// </summary>
    public void NextMapa()
    {
        mapasPrefab[currentIndex].SetActive(false);

        if (currentIndex + 1 == mapasPrefab.Length)
        {
            currentIndex = 0;
        }
        else
            currentIndex++;

        mapasPrefab[currentIndex].SetActive(true);
        CheckIfMapIsUnlocked();
    }

    /// <summary>
    /// Metodo para selecionar o mapa anterior
    /// </summary>
    public void PreviousMapa()
    {
        mapasPrefab[currentIndex].SetActive(false);

        if (currentIndex - 1 == -1)
        {
            currentIndex = mapasPrefab.Length - 1;
        }
        else
        {
            currentIndex--;
        }

        mapasPrefab[currentIndex].SetActive(true);
        CheckIfMapIsUnlocked();
    }

    /// <summary>
    /// Metodo para verificar se o mapa ja foi desbloqueado
    /// </summary>
    public void CheckIfMapIsUnlocked()
    {
        if (_mapas[currentIndex])
        {
            iconPreco.gameObject.SetActive(false);
            txtPreco.gameObject.SetActive(false);

            if (currentIndex == Home.Instance.selectedIndexMapa)
            {
                btnComprar.gameObject.GetComponent<Image>().sprite = spriteComprado;
                txtComprado.gameObject.SetActive(true);
                txtComprado.text = "SELECIONADO";
                Home.Instance.MudarCenarioPrincipal();
            }
            else
            {
                btnComprar.gameObject.GetComponent<Image>().sprite = spriteSelecionar;
                txtComprado.text = "SELECIONAR";
                txtComprado.gameObject.SetActive(true);

            }
        }
        else
        {
            btnComprar.gameObject.GetComponent<Image>().sprite = spriteSelecionar;

            iconPreco.gameObject.SetActive(true);
            txtPreco.gameObject.SetActive(true);
            txtComprado.gameObject.SetActive(false);

            txtPreco.text = PrecoMapa(currentIndex).ToString();
        }
    }

    /// <summary>
    /// Metodo para Compra do mapa
    /// </summary>
    public void SelectMapa()
    {
        if (!_mapas[currentIndex])
        {
            if (currentIndex != Home.Instance.selectedIndexMapa)
            {
                if (Home.Instance.totalDinheiro >= PrecoMapa(currentIndex))
                {
                    btnComprar.gameObject.GetComponent<Image>().sprite = spriteComprado;

                    iconPreco.gameObject.SetActive(false);
                    txtPreco.gameObject.SetActive(false);

                    txtComprado.gameObject.SetActive(true);
                    txtComprado.text = "SELECIONAR";
                    _mapas[currentIndex] = true;

                    Home.Instance.totalDinheiro -= PrecoMapa(currentIndex);
                    Home.Instance.selectedIndexMapa = currentIndex;
                    Home.Instance.mapas = _mapas;

                    Home.Instance.SaveGameData(true, PrecoMapa(currentIndex));
                    Home.Instance.CarregarDadosIniciais();                    
                }
                else
                {
                    btnComprarAnimator.SetTrigger("NoGold");
                }
            }
        }
        else
        {
            iconPreco.gameObject.SetActive(false);
            txtPreco.gameObject.SetActive(false);

            btnComprar.gameObject.GetComponent<Image>().sprite = spriteComprado;
            txtComprado.gameObject.SetActive(true);
            txtComprado.text = "SELECIONADO";
            
            Home.Instance.selectedIndexMapa = currentIndex;
            Home.Instance.SaveGameData(false, 0);
            Home.Instance.CarregarDadosIniciais();
            Home.Instance.MudarCenarioPrincipal();
        }
    }

    /// <summary>
    /// Metodo para especificar o preco do mapa
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public int PrecoMapa(int id)
    {
        int preco = 0;

        switch (id)
        {
            case 1:
                preco = 1500;
                break;
            case 2:
                preco = 2500;
                break;
            case 3:
                preco = 3500;
                break;

        }

        return preco;
    }
}
