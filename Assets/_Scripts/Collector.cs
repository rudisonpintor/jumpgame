﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collector : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D col)
    {
        switch (col.gameObject.tag)
        {
            case "BG":
                col.gameObject.SetActive(false);
                break;
            case "Platform":

                PlataformSpawner.instance.ReturnObjectToPool(col.gameObject);

                break;
            case "PlataformaEsquerda":

                PlataformSpawner.instance.ReturnObjectToPool(col.gameObject);

                break;
            //case "OnePush":
            //    col.gameObject.SetActive(false);
            //    break;
            //case "ExtraPush":
            //    col.gameObject.SetActive(false);
            //    break;
            case "Monster":
                col.gameObject.SetActive(false);
                break;
        }
    }
}
