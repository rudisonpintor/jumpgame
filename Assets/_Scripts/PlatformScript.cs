﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformScript : MonoBehaviour
{
    [SerializeField] private GameObject[] onePush, extraPush;
    [SerializeField] private Transform spawnPointPush;

    void Start()
    {
        GameObject newOnePush = null;

        if (Random.Range(0, 10) > 3)
        {
            newOnePush = Instantiate(onePush[Random.Range(0, onePush.Length)], spawnPointPush.position, Quaternion.identity);
        }
        else
        {
            newOnePush = Instantiate(extraPush[Random.Range(0, extraPush.Length)], spawnPointPush.position, Quaternion.identity);
        }

        newOnePush.transform.parent = transform;
    }
}
