﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveOffSet : MonoBehaviour
{
    private Renderer meshRender;
    private Material material;

    public float incrementoOffSet;
    public float velocidade;
    public string sortingLayer;
    public int orderInLayer;
    private float offSet;

    void Start()
    {
        meshRender = GetComponent<MeshRenderer>();
        meshRender.sortingLayerName = sortingLayer;
        meshRender.sortingOrder = orderInLayer;
        material = meshRender.material;
    }

    void FixedUpdate()
    {
        offSet += incrementoOffSet;
        material.SetTextureOffset("_MainTex", new Vector2(0, offSet * velocidade * Time.deltaTime));
    }
}
