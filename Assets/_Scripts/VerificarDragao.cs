﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerificarDragao : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("Monster"))
        {
            PlataformSpawner.instance.ReturnObjectMonsterToPool(col.gameObject);
            //PlataformSpawner.instance.SpawnMonster();
        }
    }
}
