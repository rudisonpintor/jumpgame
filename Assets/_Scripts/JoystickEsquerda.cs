﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class JoystickEsquerda : MonoBehaviour
{
    public GameObject playerPrefab;
    private EventTrigger eventTriggerDireita;
    private EventTrigger.Entry entry = new EventTrigger.Entry();
    private EventTrigger.Entry entry2 = new EventTrigger.Entry();
    private PlayerScript playerScript;

    void Start()
    {        
        playerPrefab = GameObject.FindGameObjectWithTag("Player");
        eventTriggerDireita = GetComponent<EventTrigger>();
        playerScript = playerPrefab.GetComponent<PlayerScript>();

        //entry.eventID = EventTriggerType.PointerDown;
        //entry.callback.AddListener((data) => { OnPointerDown((PointerEventData)data); });

        //entry2.eventID = EventTriggerType.PointerUp;
        //entry2.callback.AddListener((data) => { OnPointerUp((PointerEventData)data); });

        //eventTriggerDireita.triggers.Add(entry);
        //eventTriggerDireita.triggers.Add(entry2);
    }

    public void MoverParaEsquerda()
    {
        playerScript.MoverParaEsquerda();
    }

    //public void OnPointerDown(PointerEventData data)
    //{
    //    playerScript.MoverParaEsquerda();
    //}

    //public void OnPointerUp(PointerEventData data)
    //{
    //    playerScript.MoverParaEsquerda();
    //}

}
