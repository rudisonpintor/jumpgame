﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private Transform target;
    private bool isFollowPlayer;
    public float minYTreshold = -3.6f;

    private void Awake()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        if (!GameManager.Instance.player.isPlayerDead)
            Follow();
    }

    void Follow()
    {
        if (target.position.y < (transform.position.y - minYTreshold))
            isFollowPlayer = false;

        if (target.position.y > transform.position.y)
            isFollowPlayer = true;

        if (isFollowPlayer)
        {
            Vector3 temp = transform.position;
            temp.y = target.position.y;
            transform.position = temp;
        }
    }

    
}
