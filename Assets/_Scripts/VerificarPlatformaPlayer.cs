﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerificarPlatformaPlayer : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D col)
    {
        switch (col.gameObject.tag)
        {
            case "Platform":

                PlataformSpawner.instance.ReturnObjectToPool(col.gameObject);

                break;
            case "PlataformaEsquerda":

                PlataformSpawner.instance.ReturnObjectToPool(col.gameObject);

                break;

            case "Monster":

                PlataformSpawner.instance.ReturnObjectMonsterToPool(col.gameObject);

                break;
        }
    }
}
