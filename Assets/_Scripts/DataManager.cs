﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public static DataManager Instance;

    private string path = "/GameData.dat";

    private PlayerData data;
    [HideInInspector] public int selectedIndex;
    [HideInInspector] public int selectedIndexMapa;
    [HideInInspector] public bool[] heroes;
    [HideInInspector] public bool[] mapas;

    private void Awake()
    {
        MakeSingleton();

        InicializaComponentes();
    }

    void MakeSingleton()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    void InicializaComponentes()
    {
        LoadGame();

        if (data == null)
        {
            data = new PlayerData();

            data.QtdMetrosScore = 0;
            data.QtdColetadaItem1 = 0;
            data.QtdColetadaItem2 = 0;
            data.QtdColetadaItem3 = 0;
            data.QtdColetadaItem4 = 0;
            data.QtdColetadaItem5 = 0;
            data.QtdColetadaItem6 = 0;
            data.QtdColetadaItem7 = 0;
            data.QtdColetadaItem8 = 0;
            data.QtdColetadaSuper = 0;
            data.QtdMonsterKilled = 0;
            data.SoundFX = 1;
            data.SoundMusic = 1;
            data.SensibilidadeGame = .5f;
            data.TotalDinheiro = 0;
            data.ContadorMortePropaganda = 0;

            heroes = new bool[9];
            heroes[0] = true;

            mapas = new bool[4];
            mapas[0] = true;

            for (int i = 1; i < heroes.Length; i++)
            {
                heroes[i] = false;
            }

            for (int i = 1; i < mapas.Length; i++)
            {
                mapas[i] = false;
            }

            data.Heroes = heroes;
            data.Mapas = mapas;

            selectedIndex = 0;
            selectedIndexMapa = 0;

            data.SelectedIndex = selectedIndex;
            data.SelectedIndexMapa = selectedIndexMapa;

            SaveGameData(data);
        }
    }

    /// <summary>
    /// Retorna Todos os Dados Salvos
    /// </summary>
    /// <returns></returns>
    public PlayerData LoadGame()
    {
        FileStream file = null;

        try
        {
            BinaryFormatter bf = new BinaryFormatter();

            file = File.Open(Application.persistentDataPath + path, FileMode.Open);

            data = new PlayerData();
            data = (PlayerData)bf.Deserialize(file);

        }
        catch (System.Exception)
        {
        }
        finally
        {
            if (file != null)
            {
                file.Close();
            }
        }

        return data;
    }

    public void SaveGameData(PlayerData dataParam)
    {
        FileStream file = null;

        try
        {
            BinaryFormatter bg = new BinaryFormatter();

            file = File.Create(Application.persistentDataPath + path);

            if (data != null)
            {
                data.SoundFX = dataParam.SoundFX;
                data.SoundMusic = dataParam.SoundMusic;
                data.SensibilidadeGame = dataParam.SensibilidadeGame;
                data.Heroes = dataParam.Heroes;
                data.SelectedIndex = dataParam.SelectedIndex;
                data.SelectedIndexMapa = dataParam.SelectedIndexMapa;
                data.Mapas = dataParam.Mapas;

                data.QtdColetadaItem1 = dataParam.QtdColetadaItem1;
                data.QtdColetadaItem2 = dataParam.QtdColetadaItem2;
                data.QtdColetadaItem3 = dataParam.QtdColetadaItem3;
                data.QtdColetadaItem4 = dataParam.QtdColetadaItem4;
                data.QtdColetadaItem5 = dataParam.QtdColetadaItem5;
                data.QtdColetadaItem6 = dataParam.QtdColetadaItem6;
                data.QtdColetadaItem7 = dataParam.QtdColetadaItem7;
                data.QtdColetadaItem8 = dataParam.QtdColetadaItem8;
                data.QtdColetadaSuper = dataParam.QtdColetadaSuper;

                data.QtdMetrosScore = dataParam.QtdMetrosScore;
                data.TotalDinheiro = dataParam.TotalDinheiro;
                data.ContadorMortePropaganda = dataParam.ContadorMortePropaganda;

                bg.Serialize(file, data);
            }
        }
        catch (System.Exception)
        {
        }
        finally
        {
            if (file != null)
            {
                file.Close();
            }
        }
    }
}
