﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour
{
    public static LoadingScreen Instance;

    [SerializeField] private GameObject painelLoadingBar;
    [SerializeField] private Image loadingBarProgress;

    private float progressValue = 1.1f;
    [SerializeField] private float progressMultiplicador1 = 0.5f;
    [SerializeField] private float progressMultiplicador2 = 0.07f;
    public float timeToLoad;

    private void Awake()
    {
        MakeSingleton();
    }

    void Start()
    {
        //StartCoroutine(LoadingLevelRoutine());
    }

    private void Update()
    {
        ShowLoadingScreen();
    }

    void MakeSingleton()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void LoadLevel(string levelName)
    {
        painelLoadingBar.gameObject.SetActive(true);

        progressValue = 0f;

        //Time.timeScale = 0;

        SceneManager.LoadScene(levelName);
    }

    void ShowLoadingScreen()
    {
        if (progressValue < 1f)
        {
            progressValue += progressMultiplicador1 * progressMultiplicador2;
            loadingBarProgress.fillAmount = progressValue;

            //finaliza o carregamento
            if (progressValue >= 1f)
            {
                progressValue = 1.1f;

                loadingBarProgress.fillAmount = 0f;
                painelLoadingBar.SetActive(false);

                Time.timeScale = 0f;
            }
        }
    }

    IEnumerator LoadingLevelRoutine()
    {
        yield return new WaitForSeconds(timeToLoad);
        //LoadLevel("GamePlay");
        LoadLevelAsync("GamePlay");
    }

    public void LoadLevelAsync(string levelName)
    {
        StartCoroutine(LoadAsyncLevelRoutine(levelName));
    }

    IEnumerator LoadAsyncLevelRoutine(string levelName)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(levelName);
        painelLoadingBar.SetActive(true);

        while(!operation.isDone)
        {
            float progress = operation.progress / 0.9f;
            loadingBarProgress.fillAmount = progress;

            if (progress >= 1f)
            {
                painelLoadingBar.SetActive(false);
                Time.timeScale = 1f;
            }
            yield return null;
        }
    }
}
