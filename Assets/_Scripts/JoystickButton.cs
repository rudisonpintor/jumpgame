﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class JoystickButton : MonoBehaviour
{
    public GameObject playerPrefab;
    private EventTrigger eventTriggerDireita;
    private EventTrigger.Entry entry = new EventTrigger.Entry();
    private EventTrigger.Entry entry2 = new EventTrigger.Entry();

    private PlayerScript playerScript;

    void Start()
    {
        playerPrefab = GameObject.FindGameObjectWithTag("Player");
        //eventTriggerDireita = GetComponent<EventTrigger>();
        playerScript = playerPrefab.GetComponent<PlayerScript>();

        //entry.eventID = EventTriggerType.PointerUp;
        //entry.callback.AddListener((data) => { OnPointerDownDelegate((PointerEventData)data); });

        //entry2.eventID = EventTriggerType.PointerDown;
        //entry2.callback.AddListener((data) => { OnPointerUpDelegate((PointerEventData)data); });
       
        //eventTriggerDireita.triggers.Add(entry);
        //eventTriggerDireita.triggers.Add(entry2);
    }

    public void  MoverDireita()
    {
        playerScript.MoverParaDireita();
    }

    //public void OnPointerDownDelegate(PointerEventData data)
    //{
    //    data.button = PointerEventData.InputButton.Right;
    //    playerScript.MoverParaDireita();
    //}

    //public void OnPointerUpDelegate(PointerEventData data)
    //{
    //    data.button = PointerEventData.InputButton.Right;
    //    playerScript.MoverParaDireita();
    //}
}
