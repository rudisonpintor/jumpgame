﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEditor;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    private string path = "/GameData.dat";

    private PlayerData data;
    public Transform startPoint;
    private Vector3 point;
    public PlayerScript player;
    [HideInInspector] public float sensiblidadeGame;

    [Header("CONFIG PLAYERS")]
    public GameObject[] playerPrefab;
    private int currentIndexPlayer;
    public GameObject[] setasPlayers;

    [Header("HUD PAUSEGAME")]
    public GameObject painelPauseGame;
    public GameObject btnPauseGame;
    public Slider slideSensibilidade;
    public Slider slideMusic;
    public Slider slideFX;

    [Header("HUD GAMEOVER")]
    public GameObject painelGameOver;
    public Text txtScore;
    public Text txtHighScore;
    [HideInInspector] public float scoreMetros;
    private float highScoreMetros;

    [Header("HUD ITENS")]
    public Text txtMetros;
    public Text txtItensColetados;
    private int metros;
    private int itensColetados;
    public Text txtQtdColetadaItem1;
    public Text txtQtdColetadaItem2;
    public Text txtQtdColetadaItem3;
    public Text txtQtdColetadaItem4;
    public Text txtQtdColetadaItem5;
    public Text txtQtdColetadaItem6;
    public Text txtQtdColetadaItem7;
    public Text txtQtdColetadaItem8;
    public Text txtQtdColetadaSuper;
    private int qtdColetadaItem1;
    private int qtdColetadaItem2;
    private int qtdColetadaItem3;
    private int qtdColetadaItem4;
    private int qtdColetadaItem5;
    private int qtdColetadaItem6;
    private int qtdColetadaItem7;
    private int qtdColetadaItem8;
    private int qtdColatadaSuper;

    [Header("Backgrounds")]
    [SerializeField] private GameObject[] bgs;
    [SerializeField] private Sprite[] bgSprites;
    private bool mudouCenario;
    [HideInInspector] public int currentIndexMapa;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        CarregarDados();
        InstanciarPlayer();

        point = startPoint.transform.position;

        painelPauseGame.gameObject.SetActive(false);
        painelGameOver.gameObject.SetActive(false);
    }

    private void Start()
    {
        player = playerPrefab[currentIndexPlayer].GetComponent<PlayerScript>();
        MudarCenario();
        AdMobManager.Instance.OnClickShowBanner();

    }

    private void FixedUpdate()
    {
        if (!player.isPlayerDead)
            if (!player.isInitialPush)
                CalculaMetros();
    }

    /// <summary>
    /// Carrego os dados do GameManager
    /// </summary>
    void CarregarDados()
    {
        data = DataManager.Instance.LoadGame();

        if (data != null)
        {
            highScoreMetros = data.QtdMetrosScore;
            slideMusic.value = data.SoundMusic;
            slideFX.value = data.SoundFX;

            slideSensibilidade.value = data.SensibilidadeGame;
            sensiblidadeGame = data.SensibilidadeGame;
            currentIndexPlayer = data.SelectedIndex;
            currentIndexMapa = data.SelectedIndexMapa;
        }
    }

    /// <summary>
    /// Calcula os metros feito pelo player
    /// </summary>
    void CalculaMetros()
    {
        float metros = player.transform.position.y - point.y;
        float converte = Mathf.Round(metros);
        scoreMetros = converte;
        txtMetros.text = converte.ToString() + "M";
    }

    /// <summary>
    /// Restarta o Game
    /// </summary>
    public void RestarGame()
    {
        SceneManager.LoadScene("GamePlay");
        Time.timeScale = 1;
    }

    /// <summary>
    /// Pausa o Game
    /// </summary>
    public void PauseGame()
    {
        Time.timeScale = 0;
        painelPauseGame.gameObject.SetActive(true);
        btnPauseGame.gameObject.SetActive(false);
    }

    /// <summary>
    /// Despausa o Game
    /// </summary>
    public void UnpauseGame()
    {
        Time.timeScale = 1;

        painelPauseGame.gameObject.SetActive(false);
        btnPauseGame.gameObject.SetActive(true);

        SalvarConfiguracoes();
    }

    /// <summary>
    /// Redireciona para o Menu Principal
    /// </summary>
    public void BtnHome()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Home");
    }

    /// <summary>
    /// Exibi a tela de game over com a pontuacao e os itens coletados.
    /// </summary>
    public void GameOver()
    {
        //scoreMetros 30 >  highScoreMetros 20 100
        if (scoreMetros > data.QtdMetrosScore)
        {
            txtScore.text = "SCORE " + data.QtdMetrosScore.ToString() + " M";
            txtHighScore.text = "BEST SCORE <color='yellow'>" + scoreMetros.ToString() + " M" + "</color>";
        }
        else
        {
            txtScore.text = "SCORE " + scoreMetros.ToString() + " M";
            txtHighScore.text = "BEST SCORE <color='yellow'>" + highScoreMetros.ToString() + " M" + "</color>";
        }

        painelGameOver.SetActive(true);
        btnPauseGame.gameObject.SetActive(false);

        SalvarDados();

        //AdMobManager.Instance.OnClickShowBannerClose();

    }

    /// <summary>
    /// Conta a quantidade de itens coletados
    /// </summary>
    /// <param name="itemId"></param>
    public void ContarFood(int itemId)
    {
        switch (itemId)
        {
            case 1:
                qtdColetadaItem1 += 1;
                txtQtdColetadaItem1.text = qtdColetadaItem1.ToString() + "X";
                break;
            case 2:
                qtdColetadaItem2 += 1;
                txtQtdColetadaItem2.text = qtdColetadaItem2.ToString() + "X";
                break;
            case 3:
                qtdColetadaItem3 += 1;
                txtQtdColetadaItem3.text = qtdColetadaItem3.ToString() + "X";
                break;
            case 4:
                qtdColetadaItem4 += 1;
                txtQtdColetadaItem4.text = qtdColetadaItem4.ToString() + "X";
                break;
            case 5:
                qtdColetadaItem5 += 1;
                txtQtdColetadaItem5.text = qtdColetadaItem5.ToString() + "X";
                break;
            case 6:
                qtdColetadaItem6 += 1;
                txtQtdColetadaItem6.text = qtdColetadaItem6.ToString() + "X";
                break;
            case 7:
                qtdColetadaItem7 += 1;
                txtQtdColetadaItem7.text = qtdColetadaItem7.ToString() + "X";
                break;
            case 8:
                qtdColetadaItem8 += 1;
                txtQtdColetadaItem8.text = qtdColetadaItem8.ToString() + "X";
                break;
            case 9:
                qtdColatadaSuper += 1;
                txtQtdColetadaSuper.text = qtdColatadaSuper.ToString() + "X";
                break;
        }
        itensColetados += 1;
        txtItensColetados.text = itensColetados.ToString() + "X";
    }

    /// <summary>
    /// salva os dados do menu configuracoes
    /// </summary>
    void SalvarConfiguracoes()
    {
        SalvarDados();

        SoundManager.instance.soundFX.volume = data.SoundFX;
        SoundManager.instance.soundMusic.volume = data.SoundMusic;
    }

    /// <summary>
    /// Mudo o cenario de acordo com o index desbloqueado pelo player
    /// </summary>
    void MudarCenario()
    {
        switch (currentIndexMapa)
        {
            case 0:
                for (int i = 0; i < bgs.Length; i++)
                {
                    bgs[i].gameObject.GetComponent<SpriteRenderer>().sprite = bgSprites[0];
                }
                break;
            case 1:
                for (int i = 0; i < bgs.Length; i++)
                {
                    bgs[i].gameObject.GetComponent<SpriteRenderer>().sprite = bgSprites[1];
                }
                break;
            case 2:
                for (int i = 0; i < bgs.Length; i++)
                {
                    bgs[i].gameObject.GetComponent<SpriteRenderer>().sprite = bgSprites[2];
                }
                break;
            case 3:
                for (int i = 0; i < bgs.Length; i++)
                {
                    bgs[i].gameObject.GetComponent<SpriteRenderer>().sprite = bgSprites[3];
                }
                break;
        }
    }

    /// <summary>
    /// Metodo para Aumentar as Forcas dos itens de acordo com os Metros do Mapa
    /// </summary>
    void AumentarForcaItens()
    {
        switch (scoreMetros)
        {
            case 200:
                player.normalPush = 12f;
                player.extraPush = 16f;
                player.forcaSuper = 30f;
                break;
            case 400:
                player.normalPush = 14f;
                player.extraPush = 18f;
                player.forcaSuper = 50f;
                break;
            case 600:
                player.normalPush = 16f;
                player.extraPush = 20f;
                player.forcaSuper = 80f;
                break;
        }
    }

    /// <summary>
    /// Instancia o Player e o Controle de Movimento por player
    /// </summary>
    void InstanciarPlayer()
    {
        playerPrefab[currentIndexPlayer].gameObject.SetActive(true);
        setasPlayers[currentIndexPlayer].gameObject.SetActive(true);
    }

    /// <summary>
    /// Salva todos os dados no arquivo local.
    /// </summary>
    void SalvarDados()
    {
        data.QtdColetadaItem1 += qtdColetadaItem1;
        data.QtdColetadaItem2 += qtdColetadaItem2;
        data.QtdColetadaItem3 += qtdColetadaItem3;
        data.QtdColetadaItem4 += qtdColetadaItem4;
        data.QtdColetadaItem5 += qtdColetadaItem5;
        data.QtdColetadaItem6 += qtdColetadaItem6;
        data.QtdColetadaItem7 += qtdColetadaItem7;
        data.QtdColetadaItem8 += qtdColetadaItem8;
        data.QtdColetadaSuper += qtdColatadaSuper;

        data.SoundFX = slideFX.value;
        data.SoundMusic = slideMusic.value;
        data.SensibilidadeGame = slideSensibilidade.value;

        int total = data.QtdColetadaItem1 + data.QtdColetadaItem2 + data.QtdColetadaItem3 + data.QtdColetadaItem4 + data.QtdColetadaItem5 + data.QtdColetadaItem6 + data.QtdColetadaItem7 + data.QtdColetadaItem8 + data.QtdColetadaSuper;

        data.TotalDinheiro = total;
        data.ContadorMortePropaganda = 0;

        if (scoreMetros > data.QtdMetrosScore)
            data.QtdMetrosScore = scoreMetros;

        DataManager.Instance.SaveGameData(data);
        DataManager.Instance.LoadGame();
    }

    private void OnAdClosed(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Failed:
                break;
            case ShowResult.Skipped:
                break;
            case ShowResult.Finished:
                break;
            default:
                break;
        }
    }

    private void OnRewardedAdClosed(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Failed:
                break;
            case ShowResult.Skipped:
                break;
            case ShowResult.Finished:
                break;
            default:
                break;
        }
    }
}
