﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    [HideInInspector] public Rigidbody2D playerRB;
    private PlayerAnimation _playerAnim;

    public float moveSpeed = 6.0f;
    public float normalPush = 9f;
    public float extraPush = 13f;
    public float forcaSuper = 20f;

    [HideInInspector] public bool isInitialPush;
    private int pushCount;
    [HideInInspector] public bool isPlayerDead;

    [Header("SKILL")]
    [SerializeField] private GameObject[] super;
    private bool haveShield;

    private float _posYOld;
    private float _posYAtual;

    private bool isExtraPush;
    private string nomePushNow;

    void Start()
    {
        isInitialPush = true;
        playerRB = GetComponent<Rigidbody2D>();
        _playerAnim = GetComponent<PlayerAnimation>();

        _playerAnim.Fly(false);
        _playerAnim.Fall(true);
    }

    void FixedUpdate()
    {
    }

    float Sensibilidade()
    {
        return GameManager.Instance.sensiblidadeGame * 10f;
    }

    public void MoverParaDireita()
    {
        if (isInitialPush) return;

        if (!isPlayerDead)
            playerRB.velocity = new Vector2(Sensibilidade() * 1, playerRB.velocity.y);
    }

    public void MoverParaEsquerda()
    {
        if (isInitialPush) return;

        if (!isPlayerDead)
            playerRB.velocity = new Vector2(Sensibilidade() * -1, playerRB.velocity.y);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (isPlayerDead)
            return;

        switch (col.gameObject.tag)
        {
            case "ExtraPush":

                if (isInitialPush)
                {
                    isInitialPush = false;
                    playerRB.velocity = new Vector2(playerRB.velocity.x, 18f);

                    col.gameObject.SetActive(false);

                    SoundManager.instance.JumpSoundFX();
                    GameManager.Instance.ContarFood(col.gameObject.GetComponent<Item>().itemId);

                    _posYOld = transform.position.y;

                    _playerAnim.Fall(false);
                    _playerAnim.Fly(true);

                    return;
                }
                else
                {

                    playerRB.velocity = new Vector2(playerRB.velocity.x, extraPush);
                    col.gameObject.SetActive(false);

                    SoundManager.instance.JumpSoundFX();
                    GameManager.Instance.ContarFood(col.gameObject.GetComponent<Item>().itemId);

                    nomePushNow = "ExtraPush";

                    pushCount++;
                }


                break;
            case "OnePush":

                if (nomePushNow == "ExtraPush")
                    playerRB.velocity = new Vector2(playerRB.velocity.x, CalculaMediaItem());
                else
                    playerRB.velocity = new Vector2(playerRB.velocity.x, normalPush);

                col.gameObject.SetActive(false);

                SoundManager.instance.JumpSoundFX();
                GameManager.Instance.ContarFood(col.gameObject.GetComponent<Item>().itemId);

                nomePushNow = "OnePush";

                pushCount++;
                break;

            case "FallDown":
                Dead();
                break;

            case "Monster":

                if (!haveShield)
                    Dead();
                else
                    Destroy(col.gameObject);
                break;

            case "Super":

                SoundManager.instance.SuperSoundFX();

                haveShield = true;

                super[0].SetActive(true);
                super[1].SetActive(true);
                super[2].SetActive(true);
                super[3].SetActive(true);

                GameManager.Instance.ContarFood(col.gameObject.GetComponent<Item>().itemId);


                playerRB.velocity = new Vector2(playerRB.velocity.x, forcaSuper);
                playerRB.gravityScale = 0;

                StartCoroutine(SuperRoutine());
                Destroy(col.gameObject);

                break;
        }

        if (pushCount == 2)//pego a quantidade de itens coletadas e spawno novas plataformas
        {
            pushCount = 0;
            PlataformSpawner.instance.SpawnPlatforms();
        }
    }

    float CalculaMediaItem()
    {
        isExtraPush = false;
        float media = 0;

        isExtraPush = true;
        media = (normalPush + extraPush) / 2;

        return media;
    }

    void Dead()
    {
        isPlayerDead = true;
        _playerAnim.Fly(false);
        _playerAnim.Die();
        SoundManager.instance.GameOverSoundFX();
        playerRB.velocity = new Vector2(0, 0);
        GameManager.Instance.GameOver();
    }


    IEnumerator SuperRoutine()
    {
        yield return new WaitForSeconds(2.5f);
        haveShield = false;
        playerRB.gravityScale = 1;
        super[0].SetActive(false);
        super[1].SetActive(false);
        super[2].SetActive(false);
        super[3].SetActive(false);
    }

    void LiberarVoo()
    {
        _playerAnim.Jump(false);
        _playerAnim.Fall(false);
        _playerAnim.Fly(true);
    }

}
