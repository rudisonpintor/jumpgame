﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Home : MonoBehaviour
{
    public static Home Instance;

    private string path = "/GameData.dat";
    public GameObject painelScore;
    public GameObject painelConfiguracoes;
    public GameObject painelInfo;
    public GameObject painelLoja;
    public GameObject painelChar;
    public GameObject painelMapa;
    public GameObject painelFecharJogo;

    [Header("BOTOES")]
    public GameObject btnPainelScore;
    public GameObject btnPainelInfo;
    public GameObject btnPainelConfiguracoes;
    public GameObject btnPainelLoja;
    public GameObject btnPlayGame;
    public GameObject btnPainelChar;
    public GameObject btnPainelMapa;
    public GameObject btnGoldTeste;
    public GameObject btnFecharJogo;

    private PlayerData data;
    public int selectedIndex;
    public int selectedIndexMapa;
    [HideInInspector] public bool[] heroes;
    [HideInInspector] public bool[] mapas;

    public Text txtGoldLoja;

    [Header("PAINEL CONFIGURACOES")]
    public Slider slideFX;
    public Slider slideMusic;
    public Slider slideSensibilidade;
    public AudioSource audioMusic;
    public AudioSource audioPlay;

    [Header("PAINEL ITENS")]
    public Text txtMetros;
    public Text txtQtdColetadaItem1;
    public Text txtQtdColetadaItem2;
    public Text txtQtdColetadaItem3;
    public Text txtQtdColetadaItem4;
    public Text txtQtdColetadaItem5;
    public Text txtQtdColetadaItem6;
    public Text txtQtdColetadaItem7;
    public Text txtQtdColetadaItem8;
    public Text txtQtdColetadaSuper;
    public Text txtTotal;
    [HideInInspector] public int totalDinheiro;

    [Header("DADOS HEROES")]
    public Image[] spriteCharMenu;
    public GameObject[] heroesHome;
    private int currentIndex;

    [Header("DADOS MAPA")]
    public Image[] spriteMapasMenu;
    public GameObject[] mapasHome;
    private int currentIndexMapa;
    public GameObject[] backgrounds;

    private int testeCount = 0;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        InicializaComponentes();
    }

    void InicializaComponentes()
    {
        CarregarDadosIniciais();
    }

    private void Start()
    {
        MudarCenarioPrincipal();
    }

    /// <summary>
    /// Metodo para mudar o cenario principal de acordo com mapa liberado pelo player
    /// </summary>
    public void MudarCenarioPrincipal()
    {
        switch (currentIndexMapa)
        {
            case 0:
                backgrounds[0].SetActive(true);
                backgrounds[1].SetActive(false);
                backgrounds[2].SetActive(false);
                backgrounds[3].SetActive(false);
                break;
            case 1:
                backgrounds[0].SetActive(false);
                backgrounds[1].SetActive(true);
                backgrounds[2].SetActive(false);
                backgrounds[3].SetActive(false);
                break;
            case 2:
                backgrounds[0].SetActive(false);
                backgrounds[1].SetActive(false);
                backgrounds[2].SetActive(true);
                backgrounds[3].SetActive(false);
                break;
            case 3:
                backgrounds[0].SetActive(false);
                backgrounds[1].SetActive(false);
                backgrounds[2].SetActive(false);
                backgrounds[3].SetActive(true);
                break;
        }
    }

    /// <summary>
    /// Play Game
    /// </summary>
    public void BtnPlayGame()
    {
        audioPlay.Play();
        LoadingScreen.Instance.LoadLevelAsync("GamePlay");
        //SceneManager.LoadScene("GamePlay");
    }

    /// <summary>
    /// Abrir Painel Score
    /// </summary>
    public void BtnPainelScore()
    {
        painelScore.gameObject.SetActive(true);
        DesabilitarBotoes(false);
    }

    /// <summary>
    /// Abrir Painel Configuracoes
    /// </summary>
    public void BtnPainelConfiguracoes()
    {
        painelConfiguracoes.gameObject.SetActive(true);
        DesabilitarBotoes(false);
    }

    /// <summary>
    /// Abrir Painel Info
    /// </summary>
    public void BtnPainelInfo()
    {
        painelInfo.gameObject.SetActive(true);
        DesabilitarBotoes(false);
    }

    /// <summary>
    /// Abrir Painel Loja
    /// </summary>
    public void BtnPainelLoja()
    {
        painelLoja.gameObject.SetActive(true);
        DesabilitarBotoes(false);
    }

    /// <summary>
    /// Abrir Painel de Char
    /// </summary>
    public void BtnPainelChar()
    {
        painelChar.gameObject.SetActive(true);
        DesabilitarBotoes(false);
        CarregarSpriteChar(currentIndex);
    }

    /// <summary>
    /// Abrir Painel Mapa
    /// </summary>
    public void BtnPainelMapa()
    {
        painelMapa.gameObject.SetActive(true);
        DesabilitarBotoes(false);
    }

    /// <summary>
    /// Botao para abrir painel de Sair
    /// </summary>
    public void BtnPainelFecharJogo()
    {
        painelFecharJogo.gameObject.SetActive(true);
        DesabilitarBotoes(false);
    }
    /// <summary>
    /// Metodos para Dar gold pela prieira vez 10000
    /// </summary>
    public void BtnGoldTeste()
    {
        if (testeCount == 0 && data.QtdMetrosScore == 0)
        {
            testeCount = 1;

            totalDinheiro += 10000;
            SalvarConfiguracoes();
            btnGoldTeste.SetActive(false);
        }
    }

    /// <summary>
    /// Metodo para cancelar o fechamento do jogo
    /// </summary>
    public void CancelarFecharJogo()
    {
        DesabilitarBotoes(true);
        painelFecharJogo.gameObject.SetActive(false);
    }

    public void FecharJogo()
    {
        Application.Quit();
    }

    /// <summary>
    /// Metodo para fechar qualquer painel
    /// </summary>
    public void FecharPaineis()
    {
        if (painelScore.gameObject.activeInHierarchy)
        {
            painelScore.gameObject.SetActive(false);
            DesabilitarBotoes(true);
        }
        else if (painelInfo.gameObject.activeInHierarchy)
        {
            painelInfo.gameObject.SetActive(false);
            DesabilitarBotoes(true);
        }
        else if (painelConfiguracoes.gameObject.activeInHierarchy)
        {
            painelConfiguracoes.gameObject.SetActive(false);
            DesabilitarBotoes(true);
            SalvarConfiguracoes();
        }
        else if (painelLoja.gameObject.activeInHierarchy)
        {
            painelLoja.gameObject.SetActive(false);
            DesabilitarBotoes(true);
        }
        else if (painelChar.gameObject.activeInHierarchy)
        {
            painelChar.gameObject.SetActive(false);
            DesabilitarBotoes(true);
        }
        else if (painelMapa.gameObject.activeInHierarchy)
        {
            painelMapa.gameObject.SetActive(false);
            DesabilitarBotoes(true);
        }
        else if (painelFecharJogo.gameObject.activeInHierarchy)
        {
            painelFecharJogo.gameObject.SetActive(false);
            DesabilitarBotoes(true);
        }
    }

    /// <summary>
    /// Desabilita os botoes do backgroun apos abrir qualquer janela
    /// </summary>
    /// <param name="condicao"></param>
    void DesabilitarBotoes(bool condicao)
    {
        btnPlayGame.gameObject.GetComponent<Button>().interactable = condicao;
        btnPainelScore.gameObject.GetComponent<Button>().interactable = condicao;
        btnPainelInfo.gameObject.GetComponent<Button>().interactable = condicao;
        btnPainelConfiguracoes.gameObject.GetComponent<Button>().interactable = condicao;
        btnPainelLoja.gameObject.GetComponent<Button>().interactable = condicao;
        btnPainelChar.gameObject.GetComponent<Button>().interactable = condicao;
        btnPainelMapa.gameObject.GetComponent<Button>().interactable = condicao;
        btnFecharJogo.gameObject.GetComponent<Button>().interactable = condicao;
    }

    /// <summary>
    /// Salva as configuracoes do menu configuracoes
    /// </summary>
    public void SalvarConfiguracoes()
    {
        data.SoundFX = slideFX.value;
        data.SoundMusic = slideMusic.value;
        SaveGameData(false, 0);
        CarregarDadosIniciais();
    }

    /// <summary>
    /// Carrego os mapas dos Herois
    /// </summary>
    /// <param name="index"></param>
    public void CarregarSpriteChar(int index)
    {
        for (int i = 0; i < spriteCharMenu.Length; i++)
        {
            if (i == index)
            {
                spriteCharMenu[i].enabled = true;
                heroesHome[i].GetComponent<Image>().enabled = true;
            }
            else
            {
                spriteCharMenu[i].enabled = false;
                heroesHome[i].GetComponent<Image>().enabled = false;
            }
        }
    }

    /// <summary>
    /// Carrego os Sprites do Mapa
    /// </summary>
    /// <param name="index"></param>
    public void CarregarSpriteMapa(int index)
    {
        for (int i = 0; i < spriteMapasMenu.Length; i++)
        {
            if (i == index)
            {
                spriteMapasMenu[i].enabled = true;
                mapasHome[i].GetComponent<Image>().enabled = true;
            }
            else
            {
                spriteMapasMenu[i].enabled = false;
                mapasHome[i].GetComponent<Image>().enabled = false;
            }
        }
    }

    /// <summary>
    /// Metodo para selcionar o heroi principal
    /// </summary>
    public void SelecionarCharHome()
    {
        FecharPaineis();
        selectedIndex = currentIndex;
        SaveGameData(false, 0);
        CharacterSelect.Instance.CheckIfCharIsUnlocked();
    }

    /// <summary>
    /// Metodo para selecionar o mapa no menu principal
    /// </summary>
    public void SelecionarMapaHome()
    {
        FecharPaineis();
        selectedIndexMapa = currentIndexMapa;
        SaveGameData(false, 0);
        MapaSelect.Instance.CheckIfMapIsUnlocked();
        MudarCenarioPrincipal();
    }

    /// <summary>
    /// Metodo para buscar o proximo Herói no menu inicial
    /// </summary>
    public void NextHeroHome()
    {
        heroesHome[currentIndex].GetComponent<Image>().enabled = false;

        if (currentIndex + 1 == heroesHome.Length)
        {
            currentIndex = 0;
        }
        else
            currentIndex++;

        if (heroes[currentIndex])
            heroesHome[currentIndex].GetComponent<Image>().enabled = true;
        else
            NextHeroHome();

    }

    /// <summary>
    /// Metodo para buscar o Heroi anterior no menu inicial
    /// </summary>
    public void PreviousHeroHome()
    {
        heroesHome[currentIndex].GetComponent<Image>().enabled = false;

        if (currentIndex - 1 == -1)
        {
            currentIndex = heroesHome.Length - 1;
        }
        else
        {
            currentIndex--;
        }

        if (heroes[currentIndex])
            heroesHome[currentIndex].GetComponent<Image>().enabled = true;
        else
            PreviousHeroHome();
    }

    /// <summary>
    /// Metodo para buscar o Mapa posterior no menu inicial
    /// </summary>
    public void NextMapaHome()
    {
        mapasHome[currentIndexMapa].GetComponent<Image>().enabled = false;

        if (currentIndexMapa + 1 == mapasHome.Length)
        {
            currentIndexMapa = 0;
        }
        else
            currentIndexMapa++;

        if (mapas[currentIndexMapa])
            mapasHome[currentIndexMapa].GetComponent<Image>().enabled = true;
        else
            NextMapaHome();

    }

    /// <summary>
    /// Metodo para buscar o Mapa anterior no menu inicial
    /// </summary>
    public void PreviousMapaHome()
    {
        mapasHome[currentIndexMapa].GetComponent<Image>().enabled = false;

        if (currentIndexMapa - 1 == -1)
        {
            currentIndexMapa = mapasHome.Length - 1;
        }
        else
        {
            currentIndexMapa--;
        }

        if (mapas[currentIndexMapa])
            mapasHome[currentIndexMapa].GetComponent<Image>().enabled = true;
        else
            PreviousMapaHome();
    }

    /// <summary>
    /// Metodo para dar baixa nos itens apos o player fazer a compra de algum item
    /// </summary>
    /// <param name="precoItem"></param>
    /// <returns></returns>
    List<int> ZerarItens(int precoItem)
    {
        List<int> listaItens = new List<int>();

        listaItens.Add(data.QtdColetadaItem1);
        listaItens.Add(data.QtdColetadaItem2);
        listaItens.Add(data.QtdColetadaItem3);
        listaItens.Add(data.QtdColetadaItem4);
        listaItens.Add(data.QtdColetadaItem5);
        listaItens.Add(data.QtdColetadaItem6);
        listaItens.Add(data.QtdColetadaItem7);
        listaItens.Add(data.QtdColetadaItem8);
        listaItens.Add(data.QtdColetadaSuper);

        List<int> listaItensAtualizada = new List<int>();

        foreach (var item in listaItens)
        {
            if (precoItem > 0)
                if (precoItem - item > 0)
                {
                    precoItem -= item;
                    listaItensAtualizada.Add(0);
                    //se nao sobrar zera o item
                }
                else
                {
                    int qtdAtual = 0;
                    qtdAtual = item - precoItem;
                    listaItensAtualizada.Add(qtdAtual);
                    precoItem = 0;
                    break;
                }
        }
        return listaItensAtualizada;
    }

    /// <summary>
    /// Carrego todos os dados iniciais 
    /// </summary>
    public void CarregarDadosIniciais()
    {
        data = DataManager.Instance.LoadGame();

        txtMetros.text = "BEST SCORE <color='yellow'>" + data.QtdMetrosScore.ToString() + " M" + "</color>";

        slideMusic.value = data.SoundMusic;
        slideFX.value = data.SoundFX;
        audioMusic.volume = slideMusic.value;
        slideSensibilidade.value = data.SensibilidadeGame;

        txtQtdColetadaItem1.text = data.QtdColetadaItem1.ToString() + " X";
        txtQtdColetadaItem2.text = data.QtdColetadaItem2.ToString() + " X";
        txtQtdColetadaItem3.text = data.QtdColetadaItem3.ToString() + " X";
        txtQtdColetadaItem4.text = data.QtdColetadaItem4.ToString() + " X";
        txtQtdColetadaItem5.text = data.QtdColetadaItem5.ToString() + " X";
        txtQtdColetadaItem6.text = data.QtdColetadaItem6.ToString() + " X";
        txtQtdColetadaItem7.text = data.QtdColetadaItem7.ToString() + " X";
        txtQtdColetadaItem8.text = data.QtdColetadaItem8.ToString() + " X";
        txtQtdColetadaSuper.text = data.QtdColetadaSuper.ToString() + " X";

        heroes = data.Heroes;
        selectedIndex = data.SelectedIndex;
        currentIndex = selectedIndex;

        mapas = data.Mapas;
        selectedIndexMapa = data.SelectedIndexMapa;
        currentIndexMapa = selectedIndexMapa;

        txtTotal.text = data.TotalDinheiro.ToString();
        totalDinheiro = data.TotalDinheiro;
        txtGoldLoja.text = totalDinheiro.ToString();

        CarregarSpriteChar(selectedIndex);
        CarregarSpriteMapa(selectedIndexMapa);
    }

    /// <summary>
    /// Utilizado para salvar os dados quando for compra de produto na loja
    /// </summary>
    /// <param name="isCompra"></param>
    /// <param name="precoCompra"></param>
    public void SaveGameData(bool isCompra, int precoCompra)
    {
        data.SoundFX = slideFX.value;
        data.SoundMusic = slideMusic.value;
        data.SensibilidadeGame = slideSensibilidade.value;
        data.Heroes = heroes;
        data.SelectedIndex = selectedIndex;

        if (isCompra)
        {
            int index = 0;
            foreach (var item in ZerarItens(precoCompra))
            {
                if (index == 0)
                    data.QtdColetadaItem1 = item;

                if (index == 1)
                    data.QtdColetadaItem2 = item;

                if (index == 2)
                    data.QtdColetadaItem3 = item;

                if (index == 3)
                    data.QtdColetadaItem4 = item;

                if (index == 4)
                    data.QtdColetadaItem5 = item;

                if (index == 5)
                    data.QtdColetadaItem6 = item;

                if (index == 6)
                    data.QtdColetadaItem7 = item;

                if (index == 7)
                    data.QtdColetadaItem8 = item;

                if (index == 8)
                    data.QtdColetadaSuper = item;

                index++;
            }

            index = 0;
            data.TotalDinheiro = totalDinheiro;
        }
        else
            data.TotalDinheiro = totalDinheiro;

        data.SelectedIndexMapa = selectedIndexMapa;
        data.Mapas = mapas;

        CarregarSpriteChar(selectedIndex);
        CarregarSpriteMapa(selectedIndexMapa);

        DataManager.Instance.SaveGameData(data);
    }
}
