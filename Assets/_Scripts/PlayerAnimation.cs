﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    private Animator _animator;

    private void Awake()
    {
        _animator = transform.GetChild(0).GetComponent<Animator>();
    }

    public  void Jump(bool jump)
    {
        _animator.SetBool("IsJump", jump);
    }

    public void Fall(bool fall)
    {
        _animator.SetBool("IsFall", fall);
    }


    public void Die()
    {
        _animator.SetTrigger("IsDead");
    }

    public void Fly(bool fly)
    {
        _animator.SetBool("IsFly", fly);
    }

}
