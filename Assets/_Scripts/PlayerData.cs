﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerData
{
    private float qtdMetrosScore;
    private int qtdColetadaItem1;
    private int qtdColetadaItem2;
    private int qtdColetadaItem3;
    private int qtdColetadaItem4;
    private int qtdColetadaItem5;
    private int qtdColetadaItem6;
    private int qtdColetadaItem7;
    private int qtdColetadaItem8;
    private int qtdColetadaSuper;
    private int qtdMonsterKilled;
    private float soundFX;
    private float soundMusic;
    private float sensibilidadeGame;
    private int selectedIndex;
    private bool[] heroes;
    private int totalDinheiro;
    private bool[] mapas;
    private int selectedIndexMapa;
    private int contadorMortePropaganda;

    public float QtdMetrosScore
    {
        get { return qtdMetrosScore; }
        set { qtdMetrosScore = value; }
    }

    public int QtdColetadaItem1
    {
        get { return qtdColetadaItem1; }
        set { qtdColetadaItem1 = value; }
    }

    public int QtdColetadaItem2
    {
        get { return qtdColetadaItem2; }
        set { qtdColetadaItem2 = value; }
    }

    public int QtdColetadaItem3
    {
        get { return qtdColetadaItem3; }
        set { qtdColetadaItem3 = value; }
    }

    public int QtdColetadaItem4
    {
        get { return qtdColetadaItem4; }
        set { qtdColetadaItem4 = value; }
    }

    public int QtdColetadaItem5
    {
        get { return qtdColetadaItem5; }
        set { qtdColetadaItem5 = value; }
    }

    public int QtdColetadaItem6
    {
        get { return qtdColetadaItem6; }
        set { qtdColetadaItem6 = value; }
    }

    public int QtdColetadaItem7
    {
        get { return qtdColetadaItem7; }
        set { qtdColetadaItem7 = value; }
    }

    public int QtdColetadaItem8
    {
        get { return qtdColetadaItem8; }
        set { qtdColetadaItem8 = value; }
    }

    public int QtdColetadaSuper
    {
        get { return qtdColetadaSuper; }
        set { qtdColetadaSuper = value; }
    }

    public int QtdMonsterKilled
    {
        get { return qtdMonsterKilled; }
        set { qtdMonsterKilled = value; }
    }

    public float SoundFX
    {
        get { return soundFX; }
        set { soundFX = value; }
    }

    public float SoundMusic
    {
        get { return soundMusic; }
        set { soundMusic = value; }
    }

    public float SensibilidadeGame
    {
        get { return sensibilidadeGame; }
        set { sensibilidadeGame = value; }
    }

    public int SelectedIndex
    {
        get { return selectedIndex; }
        set { selectedIndex = value; }
    }

    public bool[] Heroes
    {
        get { return heroes; }
        set { heroes = value; }
    }

    public int TotalDinheiro
    {
        get { return totalDinheiro; }
        set { totalDinheiro = value; }
    }

    public int SelectedIndexMapa
    {
        get { return selectedIndexMapa; }
        set { selectedIndexMapa = value; }
    }

    public bool[] Mapas
    {
        get { return mapas; }
        set { mapas = value; }
    }

    public int ContadorMortePropaganda
    {
        get { return contadorMortePropaganda; }
        set { contadorMortePropaganda = value; }
    }

}
