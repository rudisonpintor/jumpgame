﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoGold : MonoBehaviour
{
    public GameObject btnComprar;
    public Sprite spriteRed, spriteBlue;

    public void NoMoney()
    {
        btnComprar.gameObject.GetComponent<Image>().sprite = spriteRed;
    }

    public void Normal()
    {
        btnComprar.gameObject.GetComponent<Image>().sprite = spriteBlue;
    }
}
