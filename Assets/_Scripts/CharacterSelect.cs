﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelect : MonoBehaviour
{
    public static CharacterSelect Instance;

    public GameObject[] charsPrefab;
    private int currentIndex;
    public GameObject btnComprar;
    public Image iconPreco;
    public Text txtPreco;
    public Text txtComprado;
    public Sprite spriteComprado, spriteSelecionar;
    private bool[] _heroes;
    private Animator btnComprarAnimator;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        InitializeComponentes();
        btnComprarAnimator = btnComprar.GetComponent<Animator>();
    }

    /// <summary>
    /// Metodo para carregar os mapas liberados
    /// </summary>
    void InitializeComponentes()
    {
        currentIndex = Home.Instance.selectedIndex;

        for (int i = 0; i < charsPrefab.Length; i++)
        {
            charsPrefab[i].SetActive(false);
        }

        charsPrefab[currentIndex].SetActive(true);
        _heroes = Home.Instance.heroes;
    }

    /// <summary>
    /// Metodo para selecionar o heroi posterior
    /// </summary>
    public void NextHero()
    {
        charsPrefab[currentIndex].SetActive(false);

        if (currentIndex + 1 == charsPrefab.Length)
        {
            currentIndex = 0;
        }
        else
            currentIndex++;

        charsPrefab[currentIndex].SetActive(true);
        CheckIfCharIsUnlocked();
    }

    /// <summary>
    /// Metodo para selecionar o heroi anterior
    /// </summary>
    public void PreviousHero()
    {
        charsPrefab[currentIndex].SetActive(false);

        if (currentIndex - 1 == -1)
        {
            currentIndex = charsPrefab.Length - 1;
        }
        else
        {
            currentIndex--;
        }

        charsPrefab[currentIndex].SetActive(true);
        CheckIfCharIsUnlocked();
    }

    /// <summary>
    /// Metodo para verificar se o heroi ja foi desbloqueado
    /// </summary>
    public void CheckIfCharIsUnlocked()
    {
        if (_heroes[currentIndex])
        {
            iconPreco.gameObject.SetActive(false);
            txtPreco.gameObject.SetActive(false);

            if (currentIndex == Home.Instance.selectedIndex)
            {
                btnComprar.gameObject.GetComponent<Image>().sprite = spriteComprado;
                txtComprado.gameObject.SetActive(true);
                txtComprado.text = "SELECIONADO";
            }
            else
            {
                btnComprar.gameObject.GetComponent<Image>().sprite = spriteSelecionar;
                txtComprado.text = "SELECIONAR";
                txtComprado.gameObject.SetActive(true);
            }
        }
        else
        {
            btnComprar.gameObject.GetComponent<Image>().sprite = spriteSelecionar;

            iconPreco.gameObject.SetActive(true);
            txtPreco.gameObject.SetActive(true);
            txtComprado.gameObject.SetActive(false);

            txtPreco.text = PrecoHero(currentIndex).ToString();
        }
    }

    /// <summary>
    /// Metodo para Compra do heroi
    /// </summary>
    public void SelectHero()
    {
        if (!_heroes[currentIndex])
        {
            if (currentIndex != Home.Instance.selectedIndex)
            {
                if (Home.Instance.totalDinheiro >= PrecoHero(currentIndex))
                {
                    btnComprar.gameObject.GetComponent<Image>().sprite = spriteComprado;

                    iconPreco.gameObject.SetActive(false);
                    txtPreco.gameObject.SetActive(false);

                    txtComprado.gameObject.SetActive(true);
                    txtComprado.text = "SELECIONAR";
                    _heroes[currentIndex] = true;

                    Home.Instance.totalDinheiro -= PrecoHero(currentIndex);
                    Home.Instance.selectedIndex = currentIndex;
                    Home.Instance.heroes = _heroes;

                    Home.Instance.SaveGameData(true, PrecoHero(currentIndex));
                    Home.Instance.CarregarDadosIniciais();
                }
                else
                {
                    btnComprarAnimator.SetTrigger("NoGold");
                }
            }
        }
        else
        {
            iconPreco.gameObject.SetActive(false);
            txtPreco.gameObject.SetActive(false);

            btnComprar.gameObject.GetComponent<Image>().sprite = spriteComprado;
            txtComprado.gameObject.SetActive(true);
            txtComprado.text = "SELECIONADO";

            Home.Instance.selectedIndex = currentIndex;
            Home.Instance.SaveGameData(false, 0);
            Home.Instance.CarregarDadosIniciais();
        }
    }

    /// <summary>
    /// Metodo para especificar o preco do heroi
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public int PrecoHero(int id)
    {
        int preco = 0;

        switch (id)
        {
            case 1:
                preco = 750;
                break;
            case 2:
                preco = 1000;
                break;
            case 3:
                preco = 1250;
                break;
            case 4:
                preco = 1500;
                break;
            case 5:
                preco = 1750;
                break;
            case 6:
                preco = 2000;
                break;
            case 7:
                preco = 3500;
                break;
            case 8:
                preco = 5000;
                break;
        }

        return preco;
    }
}


