﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGSpawner : MonoBehaviour
{
    private float height;
    private float highestYPos;
    public GameObject[] backgrounds;

    private void Awake()
    {
        height = backgrounds[0].GetComponent<BoxCollider2D>().bounds.size.y;// Pega o tamanho do colisor em Y
        highestYPos = backgrounds[0].transform.position.y;

        for (int i = 1; i < backgrounds.Length; i++)
        {
            if (backgrounds[i].transform.position.y > highestYPos)
                highestYPos = backgrounds[i].transform.position.y;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "BG")
        {
            if (col.transform.position.y >= highestYPos)
            {
                Vector3 temp = col.transform.position;//armazeno a posicao do ultimo BG

                for (int i = 0; i < backgrounds.Length; i++)
                {
                    if (!backgrounds[i].activeInHierarchy)
                    {
                        temp.y += height;
                        backgrounds[i].transform.position = temp;
                        backgrounds[i].gameObject.SetActive(true);

                        highestYPos = temp.y;
                    }
                }
            }
        }
    }
}
