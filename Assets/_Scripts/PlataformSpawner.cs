﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformSpawner : MonoBehaviour
{
    public static PlataformSpawner instance;

    [SerializeField] private GameObject[] _prefabPlataformMap1;
    [SerializeField] private GameObject[] _prefabPlataformMap2;
    [SerializeField] private GameObject[] _prefabPlataformMap3;
    [SerializeField] private GameObject[] _prefabPlataformMap4;
    [SerializeField] private GameObject[] monsterPrefab;

    public float monsterY = 5f;

    [SerializeField] private GameObject superPrefab;
    public float superY = 10f;
    private float superXmin = -2.61f, superXMax = 2.61f;

    private float yTreshHold = 2.6f;
    private float lastY;

    public int spawnCount = 8;
    private int platformSpawned;
    private int indexMapa;

    [SerializeField] private Transform platformParent;

    [SerializeField] private Queue<GameObject> _queuePlataformMap1;
    [SerializeField] private Queue<GameObject> _queuePlataformMap2;
    [SerializeField] private Queue<GameObject> _queuePlataformMap3;
    [SerializeField] private Queue<GameObject> _queuePlataformMap4;
    [SerializeField] private Queue<GameObject> _queuemonsterPrefab;

    [SerializeField] private int poolSizePlatforms;
    [SerializeField] private int poolSizeMonsters;

    [Header("Novo Sistema Spawn De Plataforams")]
    private int indexPlatforma;
    private float localPlatPosX_1 = 0f;
    private float localPlatPosX_2 = -1.50f;
    private float localPlatPosX_3 = 1.50f;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    void Start()
    {
        lastY = transform.position.y;
        indexMapa = GameManager.Instance.currentIndexMapa;//Pego o index atual do mapa selecionado pelo player no menu principal

        InitializePoolMonsters();//Inicializo o pool dos monstros
        InitializePoolPlataformas();

        SpawnPlatforms();
    }

    /// <summary>
    /// Carrego as plataformas na memoria para serem reutilizados
    /// </summary>
    void InitializePoolPlataformas()
    {
        switch (indexMapa)
        {
            case 0:

                _queuePlataformMap1 = new Queue<GameObject>();

                for (int i = 0; i < poolSizePlatforms; i++)
                {
                    GameObject newPlatform = Instantiate(_prefabPlataformMap1[Random.Range(0, _prefabPlataformMap1.Length)]);

                    _queuePlataformMap1.Enqueue(newPlatform);
                    newPlatform.transform.parent = platformParent;
                    newPlatform.SetActive(false);
                }

                break;
            case 1:

                _queuePlataformMap2 = new Queue<GameObject>();

                for (int i = 0; i < poolSizePlatforms; i++)
                {
                    GameObject newPlatform = Instantiate(_prefabPlataformMap2[Random.Range(0, _prefabPlataformMap2.Length)]);

                    _queuePlataformMap2.Enqueue(newPlatform);
                    newPlatform.transform.parent = platformParent;
                    newPlatform.SetActive(false);
                }

                break;
            case 2:

                _queuePlataformMap3 = new Queue<GameObject>();

                for (int i = 0; i < poolSizePlatforms; i++)
                {
                    GameObject newPlatform = Instantiate(_prefabPlataformMap3[Random.Range(0, _prefabPlataformMap3.Length)]);

                    _queuePlataformMap3.Enqueue(newPlatform);
                    newPlatform.transform.parent = platformParent;
                    newPlatform.SetActive(false);
                }

                break;
            case 3:

                _queuePlataformMap4 = new Queue<GameObject>();

                for (int i = 0; i < poolSizePlatforms; i++)
                {
                    GameObject newPlatform = Instantiate(_prefabPlataformMap4[Random.Range(0, _prefabPlataformMap4.Length)]);

                    _queuePlataformMap4.Enqueue(newPlatform);
                    newPlatform.transform.parent = platformParent;
                    newPlatform.SetActive(false);
                }

                break;
        }
    }

    /// <summary>
    /// Metodo para iniciar os Monstros da fase
    /// </summary>
    void InitializePoolMonsters()
    {
        _queuemonsterPrefab = new Queue<GameObject>();

        for (int i = 0; i < poolSizeMonsters; i++)
        {
            GameObject newMonster = Instantiate(monsterPrefab[Random.Range(0, monsterPrefab.Length)]);

            _queuemonsterPrefab.Enqueue(newMonster);
            newMonster.transform.parent = platformParent;
            newMonster.SetActive(false);
        }
    }

    /// <summary>
    /// Spawna as plataformas utilizando o poool de objetos
    /// </summary>
    public void SpawnPlatforms()
    {
        Vector2 temp = Vector2.zero;
        GameObject newPlatform = null;

        for (int i = 0; i < spawnCount; i++)
        {
            temp.y = lastY;

            int spawn = Random.Range(1, 4);//sorteio as posicoes para as novas plataformas
            indexPlatforma = spawn;

            switch (spawn)
            {
                case 1:
                    temp.x = localPlatPosX_1;
                    break;
                case 2:
                    temp.x = localPlatPosX_2;
                    break;
                case 3:
                    temp.x = localPlatPosX_3;
                    break;
            }

            newPlatform = GetPlatformPool(temp, Quaternion.identity);

            if (newPlatform.transform.childCount == 3)//Quando o player coleta o item no seu trigger ele desabilita o item, aqui é habilitado novamente
                if (!newPlatform.transform.GetChild(2).gameObject.activeInHierarchy)
                    newPlatform.transform.GetChild(2).gameObject.SetActive(true);

            if (Random.Range(0, 10) > 6)// 0 a 10 40% de spawn de mobs
                SpawnMonster();

            lastY += yTreshHold;
            platformSpawned++;
        }

        if (Random.Range(0, 100) > 85)
        {
            SpawnSuper();
        }
    }

    /// <summary>
    /// Retorno todas as plataforma por tipo de mapa e tipo de lado
    /// </summary>
    /// <returns></returns>
    public GameObject GetPlatformPool(Vector2 targetPos, Quaternion quaternion)
    {
        GameObject temp = null;

        switch (indexMapa)
        {
            case 0:
                temp = _queuePlataformMap1.Dequeue();
                temp.transform.position = targetPos;
                temp.transform.rotation = quaternion;
                temp.SetActive(true);
                break;
            case 1:
                temp = _queuePlataformMap2.Dequeue();
                temp.transform.position = targetPos;
                temp.transform.rotation = quaternion;
                temp.SetActive(true);
                break;
            case 2:
                temp = _queuePlataformMap3.Dequeue();
                temp.transform.position = targetPos;
                temp.transform.rotation = quaternion;
                temp.SetActive(true);
                break;
            case 3:
                temp = _queuePlataformMap4.Dequeue();
                temp.transform.position = targetPos;
                temp.transform.rotation = quaternion;
                temp.SetActive(true);
                break;
        }

        return temp;
    }

    /// <summary>
    /// Retorno o objeto adicionado na queue da plataforma esquerda
    /// </summary>
    /// <param name="go"></param>
    /// <param name="isLeftPlatform"></param>
    public void ReturnObjectToPool(GameObject go)
    {
        switch (indexMapa)
        {
            case 0:
                go.SetActive(false);
                _queuePlataformMap1.Enqueue(go);
                break;
            case 1:
                go.SetActive(false);
                _queuePlataformMap2.Enqueue(go);
                break;
            case 2:
                go.SetActive(false);
                _queuePlataformMap3.Enqueue(go);
                break;
            case 3:
                go.SetActive(false);
                _queuePlataformMap4.Enqueue(go);
                break;
        }
    }

    /// <summary>
    /// Retorno o monstro ja instanciado para reutilizacao
    /// </summary>
    /// <param name="targetPos"></param>
    /// <param name="quaternion"></param>
    /// <returns></returns>
    public GameObject GetEnemyPool(Vector2 targetPos, Quaternion quaternion)
    {
        GameObject temp = _queuemonsterPrefab.Dequeue();

        temp.transform.position = targetPos;
        temp.transform.rotation = quaternion;
        temp.SetActive(true);

        return temp;
    }

    /// <summary>
    /// Retorno o monstro para a lista de pool para reutilizar novamente
    /// </summary>
    /// <param name="go"></param>
    public void ReturnObjectMonsterToPool(GameObject go)
    {
        go.SetActive(false);
        _queuemonsterPrefab.Enqueue(go);
    }

    /// <summary>
    /// Metodo para Spawnar os mobs
    /// </summary>
    public void SpawnMonster()
    {
        Vector2 temp = transform.position;

        temp.y = lastY;

        GameObject newMonster = null;
        GameObject newMonster2 = null;

        //POSICOES DE SPAWN DAS PLATAFORAMS
        //localPlatPosX_1 = 0f;
        //localPlatPosX_2 = -1.50f;
        //localPlatPosX_3 = 1.50f;

        switch (indexPlatforma)
        {
            case 1:

                //50% de chance de spawnar 2 no index 0 da plataforma
                if (Random.Range(0, 2) > 0)
                {
                    temp.x = localPlatPosX_2;
                    newMonster = GetEnemyPool(temp, Quaternion.identity);

                    temp.x = localPlatPosX_3;
                    newMonster2 = GetEnemyPool(temp, Quaternion.identity);
                    newMonster2.gameObject.GetComponentInChildren<SpriteRenderer>().flipX = true;
                }
                else
                {
                    if (Random.Range(0, 2) > 0)//Sorteio se vai spawnar um enemy em 1.50f ou -1.50f
                    {
                        temp.x = localPlatPosX_2;
                    }
                    else
                    {
                        temp.x = localPlatPosX_3;
                    }

                    newMonster = GetEnemyPool(temp, Quaternion.identity);

                    if (temp.x == localPlatPosX_3)//uso o flip caso o inimigo for para o lado direito
                    {
                        newMonster.gameObject.GetComponentInChildren<SpriteRenderer>().flipX = true;
                        if (newMonster2 != null)
                            newMonster2.gameObject.GetComponentInChildren<SpriteRenderer>().flipX = true;
                    }
                }

                break;

            case 2:

                if (Random.Range(0, 2) > 0)//50% de chance de spawnar 2 no index 1 da plataforma
                {
                    temp.x = localPlatPosX_1;
                    newMonster = GetEnemyPool(temp, Quaternion.identity);

                    temp.x = localPlatPosX_2;
                    newMonster2 = GetEnemyPool(temp, Quaternion.identity);
                }
                else
                {
                    if (Random.Range(0, 2) > 0)//Sorteio se vai spawnar um enemy em 1.50f ou -1.50f
                    {
                        temp.x = localPlatPosX_1;
                    }
                    else
                        temp.x = localPlatPosX_2;

                    newMonster = GetEnemyPool(temp, Quaternion.identity); 
                }

                break;

            case 3:

                if (Random.Range(0, 2) > 0)//50% de chance de spawnar 2 no index 1 da plataforma
                {
                    temp.x = localPlatPosX_1;
                    newMonster = GetEnemyPool(temp, Quaternion.identity); 

                    temp.x = localPlatPosX_2;
                    newMonster2 = GetEnemyPool(temp, Quaternion.identity);
                    newMonster2.gameObject.GetComponentInChildren<SpriteRenderer>().flipX = true;
                }
                else
                {
                    if (Random.Range(0, 2) > 0)//Sorteio se vai spawnar um enemy em 1.50f ou -1.50f
                    {
                        temp.x = localPlatPosX_1;
                    }
                    else
                        temp.x = localPlatPosX_3;

                    newMonster = GetEnemyPool(temp, Quaternion.identity);

                    if (temp.x == localPlatPosX_3)//uso o flip caso o inimigo for para o lado direito
                    {
                        newMonster.gameObject.GetComponentInChildren<SpriteRenderer>().flipX = true;
                        if (newMonster2 != null)
                            newMonster2.gameObject.GetComponentInChildren<SpriteRenderer>().flipX = true;
                    }
                }

                break;
        }
    }

    /// <summary>
    /// Spawna o super com 10% de chance
    /// </summary>
    void SpawnSuper()
    {
        Vector2 temp = transform.position;
        temp.x = Random.Range(superXmin, superXMax);
        temp.y += superY;

        GameObject newSuper = Instantiate(superPrefab, temp, Quaternion.identity);
        newSuper.transform.parent = platformParent;
    }
}
